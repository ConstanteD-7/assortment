<footer class="page-footer">
    <div class="container">
        <div class="page-footer__wrapper">
            <div class="page-footer__logo">
                <?php the_custom_logo(); ?>
            </div>

            <span class="page-footer__copyright">
                Все права защищены 2022
            </span>

            <a class="nav__link" href="http://assortment/politika-konfidenczialnosti/">
                Политика конфиденциальности
            </a>

            <nav class="page-footer__nav nav">
                <ul class="nav__list list-reset">
                    <li class="nav__item"><a href="tel:+78008008080" class="nav__link">8&nbsp;800&nbsp;800
                            80&nbsp;80</a></li>
                    <li class="nav__item"><a href="https://wa.me/78008008080" class="nav__link">WhatsApp</a>
                    </li>
                    <li class="nav__item"><a href="tg://msg?text=<?php echo urlencode( '<TEXT>' ); ?>&to=78008008080"
                            class="nav__link">Telegram</a></li>
                    <li class="nav__item"><a href="#" class="nav__link">Email</a></li>
                </ul>
            </nav>
        </div>

    </div>
</footer>

<?php
     wp_footer();
    ?>

</body>

</html>
