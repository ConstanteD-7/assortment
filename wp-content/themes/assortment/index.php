<?php
get_header();
?>

<main class="page-main">

    <!-- Всплывающие формы заказов -->
    <div class="form-popup" id="form1">
        <div class="form-popup__container">
            <h2 class="form-popup__title">
                Заказ услуги «Консультация»
            </h2>
            <p class="form-popup__descr">
                Наши менеджеры примут и рассчитают ваш заказ
            </p>
            <?php echo do_shortcode('[contact-form-7 id="658" title="Форма заказа услуги"]'); ?>
            <div class="form-popup__close"></div>
        </div>
    </div>
    <div class="form-popup" id="form2">
        <div class="form-popup__container">
            <h2 class="form-popup__title">
                Наши менеджеры примут и рассчитают ваш заказ
            </h2>
            <p class="form-popup__descr">
                Оставьте ваши данные и мы перезвоним в течение 15 минут
            </p>
            <?php echo do_shortcode('[contact-form-7 id="659" title="Форма заказа услуги"]'); ?>
            <div class="form-popup__close"></div>
        </div>
    </div>


    <section class="hero">
        <div class="container hero__container">
            <div class="hero__content-wrapp">

                <h1 class="hero__title"><?php the_field('home_title'); ?></h1>
                <p class="hero__description"><?php the_field('home_descr'); ?></p>
                <a class="hero__btn btn-link"><?php the_field('text_btn'); ?></a>

            </div>
            <div class="hero__img-wrapp">
                <img src="<?php the_field('home_img'); ?>" alt="alt" class="hero__img">
            </div>
        </div>
    </section>
    <section class="guarantee">
        <div class="container guarantee__container">
            <div class="card-guarantee card-guarantee--bk1"><span
                    class="card-guarantee__header card-guarantee__header--quality"></span> <span
                    class="card-guarantee__body"><?php the_field('garant_1'); ?></span>
            </div>
            <div class="card-guarantee card-guarantee--bk2"><span
                    class="card-guarantee__header card-guarantee__header--terms"></span> <span
                    class="card-guarantee__body"><?php the_field('garant_2'); ?></span>
            </div>
            <div class="card-guarantee card-guarantee--bk3"><span
                    class="card-guarantee__header card-guarantee__header--communication"></span> <span
                    class="card-guarantee__body"><?php the_field('garant_3'); ?></span>
            </div>
        </div>
    </section>
    <section class="services__section">
        <div class="container">

            <div class="service">
                <div class="service__content"><span
                        class="service__sub-title"><?php the_field('name_section_1'); ?></span>
                    <h2 class="service__title"><?php the_field('name_services_1'); ?></h2>
                    <ul class="service__list">
                        <li class="service__item"><?php the_field('consult_1'); ?></li>
                        <li class="service__item"><?php the_field('consult_2'); ?></li>
                        <li class="service__item"><?php the_field('consult_3'); ?></li>
                    </ul>
                    <p class="service__text"><?php the_field('consult_descr_1'); ?></p>

                </div>
                <div class="service__content-img">
                    <img src="<?php the_field('img_big_1'); ?>" alt="alt" class="service__img">
                    <div class="service__wrapper">
                        <img src="<?php the_field('img_small_1'); ?>" alt="alt" class="service__img--mini">
                    </div>
                </div>
                <div class="service__link-wrapp">
                    <span class="service__sub-link"><?php the_field('price_1'); ?></span>
                    <a class="service__link btn-link"><?php the_field('services_btn_1'); ?></a>
                </div>
            </div>


            <div class="service service--reverse">
                <div class="service__content"><span
                        class="service__sub-title"><?php the_field('name_section_2'); ?></span>
                    <h2 class="service__title"><?php the_field('name_services_2'); ?></h2>
                    <p class="service__text"><?php the_field('consult_descr_2'); ?></p>
                </div>
                <div class="service__content-img">
                    <img src="<?php the_field('img_big_2'); ?>" alt="alt" class="service__img">
                    <div class="service__wrapper">
                        <img src="<?php the_field('img_small_2'); ?>" alt="alt" class="service__img--mini">
                    </div>
                </div>
                <div class="service__link-wrapp">
                    <span class="service__sub-link"><?php the_field('price_2'); ?></span>
                    <a class="service__link btn-link"><?php the_field('services_btn_2'); ?></a>
                </div>
            </div>


            <div class="service">
                <div class="service__content"><span
                        class="service__sub-title"><?php the_field('name_section_3'); ?></span>
                    <h2 class="service__title"><?php the_field('name_services_3'); ?></h2>
                    <p class="service__text"><?php the_field('consult_descr_3'); ?></p>
                </div>
                <div class="service__content-img">
                    <img src="<?php the_field('img_big_3'); ?>" alt="alt" class="service__img">
                    <div class="service__wrapper">
                        <img src="<?php the_field('img_small_2'); ?>" alt="alt" class="service__img--mini">
                    </div>
                </div>
                <div class="service__link-wrapp">
                    <span class="service__sub-link"><?php the_field('price_3'); ?></span>
                    <a class="service__link btn-link"><?php the_field('services_btn_3'); ?></a>
                </div>
            </div>

        </div>
    </section>
    <section class="advantage">
        <div class="container advantage__container">

            <p class="advantage__title"><?php the_field('descr_about'); ?></p>


            <div class="advantage__item-advantage item-advantage"><span
                    class="item-advantage__title"><?php the_field('achievement_1'); ?></span> <span
                    class="item-advantage__sub-title"><?php the_field('descr_achievement_1'); ?></span></div>
            <div class="advantage__item-advantage item-advantage"><span
                    class="item-advantage__title"><?php the_field('achievement_2'); ?></span> <span
                    class="item-advantage__sub-title"><?php the_field('descr_achievement_2'); ?></span></div>
            <div class="advantage__item-advantage item-advantage"><span
                    class="item-advantage__title"><?php the_field('achievement_3'); ?></span> <span
                    class="item-advantage__sub-title"><?php the_field('descr_achievement_3'); ?></span></div>
            <div class="advantage__item-advantage item-advantage"><span
                    class="item-advantage__title"><?php the_field('achievement_4'); ?></span> <span
                    class="item-advantage__sub-title"><?php the_field('descr_achievement_4'); ?></span></div>

        </div>
    </section>
    <section class="our-work">
        <div class="container our-work__container">

            <h2 class="our-work__title"><?php the_field('title_portfolio'); ?></h2>

            <img class="our-work__img our-work__img--one" src="<?php the_field('portfolio_img_1'); ?>" alt="alt">
            <img class="our-work__img our-work__img--two" src="<?php the_field('portfolio_img_2'); ?>" alt="alt">
            <img class="our-work__img our-work__img--three" src="<?php the_field('portfolio_img_3'); ?>" alt="alt">
            <img class="our-work__img our-work__img--four" src="<?php the_field('portfolio_img_4'); ?>" alt="alt">
            <div class="our-work__wrap">
                <a href="#" class="our-work__link btn-link"><?php the_field('btn_portfolio'); ?></a>
            </div>

        </div>
    </section>

</main>

<?php
get_footer();
?>
