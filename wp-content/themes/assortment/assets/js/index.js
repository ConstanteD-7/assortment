!(function () {
  "use strict";
  var e,
    n = {
      919: function () {
        document.addEventListener("DOMContentLoaded", function () {
          var e, n, t;
          (e = document.querySelector(".page-header__sub-burger-menu")) &&
            e.addEventListener("click", function () {
              e.classList.toggle("open");
            }),
            document.querySelector(".service") &&
              ((n = function () {
                document.querySelector(".scroll") ||
                  document.body.classList.add("scroll");
              }),
              (t = function () {
                document.querySelector(".scroll") &&
                  document.body.classList.remove("scroll");
              }),
              window.addEventListener("wheel", function () {
                n(),
                  setTimeout(function () {
                    t();
                  }, 500);
              }),
              window.addEventListener("touchmove", function () {
                n(),
                  setTimeout(function () {
                    t();
                  }, 500);
              }));
        });
      },
    },
    t = {};
  function o(e) {
    var r = t[e];
    if (void 0 !== r) return r.exports;
    var u = (t[e] = { exports: {} });
    return n[e](u, u.exports, o), u.exports;
  }
  (o.m = n),
    (e = []),
    (o.O = function (n, t, r, u) {
      if (!t) {
        var c = 1 / 0;
        for (l = 0; l < e.length; l++) {
          (t = e[l][0]), (r = e[l][1]), (u = e[l][2]);
          for (var i = !0, s = 0; s < t.length; s++)
            (!1 & u || c >= u) &&
            Object.keys(o.O).every(function (e) {
              return o.O[e](t[s]);
            })
              ? t.splice(s--, 1)
              : ((i = !1), u < c && (c = u));
          if (i) {
            e.splice(l--, 1);
            var f = r();
            void 0 !== f && (n = f);
          }
        }
        return n;
      }
      u = u || 0;
      for (var l = e.length; l > 0 && e[l - 1][2] > u; l--) e[l] = e[l - 1];
      e[l] = [t, r, u];
    }),
    (o.o = function (e, n) {
      return Object.prototype.hasOwnProperty.call(e, n);
    }),
    (function () {
      var e = { 826: 0, 499: 0 };
      o.O.j = function (n) {
        return 0 === e[n];
      };
      var n = function (n, t) {
          var r,
            u,
            c = t[0],
            i = t[1],
            s = t[2],
            f = 0;
          if (
            c.some(function (n) {
              return 0 !== e[n];
            })
          ) {
            for (r in i) o.o(i, r) && (o.m[r] = i[r]);
            if (s) var l = s(o);
          }
          for (n && n(t); f < c.length; f++)
            (u = c[f]), o.o(e, u) && e[u] && e[u][0](), (e[u] = 0);
          return o.O(l);
        },
        t = (self.webpackChunk = self.webpackChunk || []);
      t.forEach(n.bind(null, 0)), (t.push = n.bind(null, t.push.bind(t)));
    })();
  var r = o.O(void 0, [499], function () {
    return o(919);
  });
  r = o.O(r);
})();

function popup(clsasTrigger, wrapp, gallery = false, popupBody) {
  if (document.querySelectorAll(`.${wrapp}`).length > 0) {
    if (!document.querySelector(".popup")) {
      document.body.insertAdjacentHTML(
        "beforeend",
        `<div class="popup">
          <div class="popup__content">
            <button class="popup__close"></button>
            <img class="card-articles__img">
          </div>
        </div>
      `
      );
    }

    const popup = document.querySelector(".popup"),
      popupContent = popup.querySelector(".popup__content"),
      popupImg = popup.querySelector(".card-articles__img");

    if (popup && wrapp) {
      let wrapper = document.querySelectorAll(`.${wrapp}`),
        content;
      wrapper.forEach((elem, i, arr) => {
        elem.addEventListener("click", (e) => {
          let target = e.target;
          if (target.classList.contains(clsasTrigger)) {
            const scrollWidth =
              window.innerWidth - document.documentElement.clientWidth;
            document.querySelector(
              "body"
            ).style.marginRight = `${scrollWidth}px`;
            document.querySelector("body").style.overflow = "hidden";

            if (elem.querySelector("img")) {
              popupImg.src = elem.querySelector("img").src;
            }
            if (elem.querySelector(`.${popupBody}`)) {
              content = elem.querySelector(`.${popupBody}`);
              content = content.cloneNode(content);
              popupContent.appendChild(content);
            }
            popup.classList.add("activ");

            if (gallery) {
              const popupPrev = document.createElement("div"),
                popupNext = document.createElement("div");
              popupPrev.classList.add("popup-prev");
              popupNext.classList.add("popup-next");
              popup.appendChild(popupPrev);
              popup.appendChild(popupNext);

              popup.addEventListener("click", (e) => {
                if (e.target.classList.contains("popup-prev")) {
                  let leng = arr.length - 1;
                  if (i >= 1 || i == leng) {
                    popupImg.src = arr[--i].querySelector("img").src;
                  } else if (i == 0) {
                    i = leng;
                    popupImg.src = arr[i].querySelector("img").src;
                  }
                }
                if (e.target.classList.contains("popup-next")) {
                  let leng = arr.length - 1;
                  if (i > leng || i == leng) {
                    i = 0;
                    popupImg.src = arr[0].querySelector("img").src;
                  } else if (i < leng && i >= 0) {
                    popupImg.src = arr[++i].querySelector("img").src;
                  }
                }
              });
            }
          }
        });
      });

      popup.addEventListener("click", (e) => {
        let target = e.target;
        if (
          target &&
          (target.classList.contains("popup__close") ||
            target.classList.contains("popup"))
        ) {
          popup.classList.remove("activ");
          if (popupContent.querySelector(`.${popupBody}`)) {
            popupContent.removeChild(content);
          }
          setTimeout(() => {
            document.querySelector("body").style.marginRight = 0;
            document.querySelector("body").style.overflow = "";
          }, 200);
        }
      });
    }
  }
}

popup("certificate-card", "certificate-card", true);

function formPopup(sectionWrap, btnTriger, formEl, sectionTitle = "") {
  let form = document.querySelector(formEl),
    titleH2 = document.querySelector(".form-popup__title");
  sectionContain = document.querySelectorAll(sectionWrap);
  sectionContain.forEach((el) => {
    el.addEventListener("click", function (e) {
      if (e.target.classList.contains(btnTriger)) {
        if (sectionTitle.length > 0) {
          let titleSec = el.querySelector(`.${sectionTitle}`);
          let text = titleSec.textContent;
          titleH2.textContent = `Заказ услуги «${text}»`;
        }

        form.classList.add("active");
        const scrollWidth =
          window.innerWidth - document.documentElement.clientWidth;
        document.querySelector("body").style.marginRight = `${scrollWidth}px`;
        document.querySelector(".page").classList.add("scroll-stop");
      }
    });
  });

  form.addEventListener("click", function (e) {
    if (
      e.target.classList.contains("form-popup__close") ||
      e.target.classList.contains("form-popup")
    ) {
      form.classList.remove("active");
      document.querySelector(".page").classList.remove("scroll-stop");
      document.querySelector("body").style.marginRight = 0;
    }
  });
}

if (document.querySelector(".hero__btn") && document.querySelector("#form2")) {
  formPopup(".hero", "hero__btn", "#form2");
}
if (
  document.querySelector(".service__link") &&
  document.querySelector("#form1")
) {
  formPopup(".service", "service__link", "#form1", "service__title");
}
