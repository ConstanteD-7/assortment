<?php
/*
  Template Name: О компании
  */
?>

<?php
get_header();
?>

<main class="page-main">
    <section class="about">
        <div class="container">
            <div class="row">
                <div class="about__content">
                    <div class="row align-items">
                        <img class="about_img col-lg-7 col-md-7" src="<?php the_field('about_img'); ?>">
                        <div class="col-lg-5 col-md-5 about__content-left">

                            <h1 class="about__title title">
                                <?php the_field('about_title'); ?>
                            </h1>

                            <p class="descr">
                                <?php the_field('about_descr'); ?>
                            </p>

                            <a class="about__btn btn-link" href="">
                                <?php the_field('about_text_btn'); ?>
                            </a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

<!--     <section class="result">
        <div class="container">
            <div class="row">
                <div class="result__content">
                    <p class="about__description">
                        <?php the_field('descr_about'); ?>
                    </p>

                    <div class="flex">
                        <div class="result__content-left col-lg-5 col-md-5">
                            <h2 class="result__title subtitle">
                                <?php the_field('result_title'); ?>
                            </h2>

                            <p class="result__descr">
                                <?php the_field('descr_result'); ?>
                            </p>
                        </div>

                        <div class="result__content-right col-lg-5 col-md-5">
                            <img class="result__img-big" src="<?php the_field('result_img_big'); ?>" alt="">
                            <img class="result__img-small" src="<?php the_field('result_img_small'); ?>" alt="">
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section> -->

    <section class="team">
        <div class="container">
            <div class="row">
                <h2 class="team__title subtitle">
                    <?php the_field('team_title'); ?>
                </h2>


                <?php
                // параметры по умолчанию
                $my_posts = get_posts(array(
                    'numberposts' => -1,
                    'category_name'    => 'team_people',
                    'orderby'     => 'date',
                    'order'       => 'ASC',
                    'post_type'   => 'post',
                    'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                ));



                foreach ($my_posts as $post) {
                    setup_postdata($post);
                ?>

                    <div class="team__content row">
                        <div class="team__content-left  col-md-5 col-lg-5">
                            <img class="team__img" src="<?php the_field('team_img'); ?>">
                        </div>
                        <div class="team__content-right col-lg-7 col-md-7">
                            <h3 class="team_subtitle">
                                <?php the_field('name_people'); ?>
                            </h3>
                            <p class="team_descr">
                                <?php the_field('spec_people'); ?>
                            </p>

                            <p class="description">
                                <?php the_field('descr_people'); ?>
                            </p>
                        </div>

                    </div>

                <?php
                }

                wp_reset_postdata(); // сброс
                ?>

            </div>
    </section>

    <section class="partners">
        <div class="container">
            <div class="row">
                <h2 class="partners__title subtitle">
                    <?php the_field('partners_title'); ?>
                </h2>
                <div class="partners">
                    <div class="row">
                        <div class="partners__content-top">
                            <?php
                            // параметры по умолчанию
                            $my_posts = get_posts(array(
                                'numberposts' => -1,
                                'category_name'    => 'big_logo',
                                'orderby'     => 'date',
                                'order'       => 'ASC',
                                'post_type'   => 'post',
                                'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                            ));



                            foreach ($my_posts as $post) {
                                setup_postdata($post);
                            ?>

                                <div class="partners__logo-big">
                                    <img class="partners__img" src="<?php the_field('big_logo_partners'); ?>">
                                </div>

                            <?php
                            }

                            wp_reset_postdata(); // сброс
                            ?>
                        </div>
                    </div>


                    <div class="row ">
                        <div class="partners__content-bottom">
                            <?php
                            // параметры по умолчанию
                            $my_posts = get_posts(array(
                                'numberposts' => -1,
                                'category_name'    => 'small_logo',
                                'orderby'     => 'date',
                                'order'       => 'ASC',
                                'post_type'   => 'post',
                                'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                            ));



                            foreach ($my_posts as $post) {
                                setup_postdata($post);
                            ?>


                                <div class="partners__logo-small">
                                    <img class="partners__img" src="<?php the_field('small_logo_partners'); ?>">
                                </div>

                            <?php
                            }

                            wp_reset_postdata(); // сброс
                            ?>
                        </div>



                    </div>

                </div>
            </div>
    </section>

</main>

<?php
get_footer();
?>
