<?php
/*
  Template Name: Цены
  */
?>

<?php
get_header();
?>

<main class="page-main">

    <sectionc class="price">
        <div class="container price__container">
            <div class="row">
                <h1 class="price__title title">
                    <?php the_field('price_title'); ?>
                </h1>
                <p class="price_descr">
                    <?php the_field('price_descr'); ?>
                </p>
            </div>

            <div class="price-table">
                <div class="price__row">
                    <div class="price_icon-1"></div>
                    <h2 class="price_title">
                        Детский трикотаж
                    </h2>
                </div>

                <div class="price-table__wrapp price-table__wrapp--header">
                    <span class="price-table__title">Вид изделия</span>
                    <span class="price-table__col" data-label="Построение базовой конструкции">Построение базовой
                        конструкции</span>
                    <span class="price-table__col" data-label="Градация лекал 15% от стоимости">Градация лекал 15% от
                        стоимости</span>
                </div>

                <?php
                // параметры по умолчанию
                $my_posts = get_posts(array(
                    'numberposts' => -1,
                    'category_name'    => 'child_product',
                    'orderby'     => 'date',
                    'order'       => 'ASC',
                    'post_type'   => 'post',
                    'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                ));



                foreach ($my_posts as $post) {
                    setup_postdata($post);
                ?>

                    <div class="price-table__wrapp">
                        <span class="price-table__title"><?php the_field('type_product_child'); ?></span>
                        <span class="price-table__col" data-label="Построение базовой конструкции"><?php the_field('base_child'); ?></span>
                        <span class="price-table__col" data-label="Градация лекал 15% от стоимости"><?php the_field('price_child'); ?></span>
                    </div>

                <?php
                }

                wp_reset_postdata(); // сброс
                ?>

            </div>

            <div class="price-table">
                <div class="price__row">
                    <div class="price_icon-2"></div>
                    <h2 class="price_title">
                        Домашняя одежда
                    </h2>
                </div>

                <div class="price-table__wrapp price-table__wrapp--header">
                    <span class="price-table__title">Вид изделия</span>
                    <span class="price-table__col" data-label="Построение базовой конструкции">Построение базовой
                        конструкции</span>
                    <span class="price-table__col" data-label="Градация лекал 15% от стоимости">Градация лекал 15% от
                        стоимости</span>
                </div>

                <?php
                // параметры по умолчанию
                $my_posts = get_posts(array(
                    'numberposts' => -1,
                    'category_name'    => 'home_product',
                    'orderby'     => 'date',
                    'order'       => 'ASC',
                    'post_type'   => 'post',
                    'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                ));



                foreach ($my_posts as $post) {
                    setup_postdata($post);
                ?>

                    <div class="price-table__wrapp">
                        <span class="price-table__title"><?php the_field('type_product_home'); ?></span>
                        <span class="price-table__col" data-label="Построение базовой конструкции"><?php the_field('base_home'); ?></span>
                        <span class="price-table__col" data-label="Градация лекал 15% от стоимости"><?php the_field('price_home'); ?></span>
                    </div>

                <?php
                }

                wp_reset_postdata(); // сброс
                ?>

            </div>

        </div>
        </section>


        <section class="form">
            <div class="container form__wrapp">
                <div class="row">
                    <div class="form__container">
                        <div class="form__content">
                            <div class="form__content-left col-lg-6">
                                <h2 class="form__title">
                                    <?php the_field('form_title'); ?>
                                </h2>
                                <p class="form__descr">
                                    <?php the_field('form_text'); ?>
                                </p>

                                <a class="form__contact" href="tel:+73522550288"><?php the_field('form_tel'); ?></a>

                                <a class="form__contact" href="https://wa.me/79128350288"><?php the_field('form_whatsapp'); ?></a>
                            </div>
                            <div class="form__content-right col-lg-6">
                                <?php echo do_shortcode('[contact-form-7 id="184" title="Форма заявки"]'); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


</main>

<?php
get_footer();
?>