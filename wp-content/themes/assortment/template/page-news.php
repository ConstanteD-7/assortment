<?php
/*
  Template Name: Новости
  */
?>

<?php
get_header();
?>

<main class="page-main">

    <section class="news">
        <div class="container">
            <div class="row">
                <div class="news__content-top">
                    <h1 class="news__title">
                        <?php the_field('news_title'); ?>
                    </h1>
                    <p class="news__descr">
                        <?php the_field('news_descr'); ?>
                    </p>
                </div>

                <div class="news__content-bottom">
                    <?php
                    // параметры по умолчанию
                    $my_posts = get_posts(array(
                        'numberposts' => -1,
                        'category_name'    => 'news',
                        'orderby'     => 'date',
                        'order'       => 'ASC',
                        'post_type'   => 'post',
                        'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                    ));

                    foreach ($my_posts as $post) {
                        setup_postdata($post);
                    ?>

                        <a class="catalog__card-link" href="<?php echo get_permalink(); ?>">
                            <div class="news__card">

                                <img class="news_img" src="<?php the_field('news_img'); ?>">

                                <div class="news__content">
                                    <div class="news__border"></div>
                                    <p class="news__content-title">
                                        <?php the_field('news_card-title'); ?>
                                    </p>

                                    <a class="news__btn btn-link" href="<?php echo get_permalink(); ?>">
                                        <?php the_field('news_text-btn'); ?>
                                    </a>
                                </div>


                            </div>
                        </a>


                    <?php
                    }

                    wp_reset_postdata(); // сброс
                    ?>
                </div>
            </div>

        </div>

    </section>
</main>

<?php
get_footer();
?>