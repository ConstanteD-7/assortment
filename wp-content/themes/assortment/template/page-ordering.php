<?php
/*
  Template Name: Оформление заказа
  */
?>

<?php
get_header();
?>

<main class="page-main">

    <section class="ordering">
        <div class="container">
            <div class="row">
                <div class="contacts__content-top">
                    <h1 class="ordering__title">
                        <?php the_field('registration_title'); ?>
                    </h1>
                    <p class="reviews__descr">
                        <?php the_field('registration_descr'); ?>
                    </p>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="stage">
                            <div class="stage__header"><span>01</span>этап</div>
                            <p class="stage__text"><?php the_field('registration_stage_1'); ?></p>
                        </div>
                        <div class="stage">
                            <div class="stage__header"><span>02</span>этап</div>
                            <p class="stage__text"><?php the_field('registration_stage_2'); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <img class="stage__img" src="<?php the_field('registration_img_right'); ?>">
                    </div>


                </div>


                <div class="stage">
                    <div class="stage__header"><span>03</span>этап</div>
                    <p class="stage__text"><?php the_field('registration_stage_3'); ?></p>
                </div>
                <div class="stage">
                    <div class="stage__header"><span>04</span>этап</div>
                    <p class="stage__text"><?php the_field('registration_stage_4'); ?></p>
                </div>




                <div class="contacts-maps__wrapp-grid">
                    <div class="contacts-maps__card">
                        <h2 class="contacts-maps__title"><?php the_field('registration_form'); ?></h2>
                        <div class="contacts-map2">
                            <p class="contacts-maps__text"><?php the_field('registration_form_descr'); ?></p>
                            <a class="contacts-maps__link" href="tel:+73522550288"><?php the_field('registration_form_tel'); ?></a>
                            <a class="contacts-maps__link" href="https://wa.me/79128350288"><?php the_field('registration_form_wts'); ?>
                            </a>
                        </div>
                    </div>

                    <?php echo do_shortcode('[contact-form-7 id="656" title="Форма в контактах"]'); ?>

                    <img class="ordering__img" src="<?php the_field('registration_img_bottom'); ?>">

                </div>
            </div>
    </section>
</main>

<?php
get_footer();
?>