<?php
/*
  Template Name: Политика конфиденциальности
  */
?>

<?php
get_header();
?>

<main class="page-main">

    <section class="policy">
        <div class="container">

            <div class="contacts__content-top">
                <h1 class="ordering__title mb-50">
                    <?php the_field('policy_title'); ?>
                </h1>

            </div>
            <p class="text">
                <?php the_field('policy_descr'); ?>
            </p>
        </div>
    </section>
</main>

<?php
get_footer();
?>