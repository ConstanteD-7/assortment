<?php
/*
  Template Name: Производство
  */
?>

<?php
get_header();
?>

<main class="page-main">

  <section class="production">

    <div class="container">
      <div class="row">
        <div class="production__content">
          <img src="<?php the_field('production_img'); ?>" alt="" class="production__img">
          <div class="production__content-left">
            <h1 class="production__title">
              <?php the_field('production_title'); ?>
            </h1>
            <p class="production__descr">
              <?php the_field('production_descr'); ?>
            </p>
            <p class="production__list">
              <?php the_field('production_list'); ?>
            </p>
          </div>
        </div>
      </div>
    </div>

  </section>

  <section class="equipment">
    <div class="container">
      <div class="row">
        <h2 class="equipment__title">
          <?php the_field('equipment_title'); ?>
        </h2>
        <p class="equipment__descr">
          <?php the_field('equipment_descr'); ?>
        </p>
      </div>

      <div class="row">
        <div class="equipment__video">

          <?php
          // параметры по умолчанию
          $my_posts = get_posts(array(
            'numberposts' => -1,
            'category_name'    => 'equipment',
            'orderby'     => 'date',
            'order'       => 'ASC',
            'post_type'   => 'post',
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
          ));



          foreach ($my_posts as $post) {
            setup_postdata($post);
          ?>

            <div class="equipment__card-video">
              <video width="100%" height="215px" controls loop preload="none" poster="<?php the_field('equipment_poster'); ?>">
                <source src="<?php the_field('equipment_video'); ?>" type="video/mp4">
              </video>
              <div class="equipment__border"></div>
              <p class="equipment__subdescr">
                <?php the_field('equipment_subdescr'); ?>
              </p>
            </div>

          <?php
          }

          wp_reset_postdata(); // сброс
          ?>

        </div>
      </div>
    </div>
  </section>

  <section class="have">
    <div class="container">
      <div class="row">
        <div class="have__content">
          <h2 class="have__title">
            <?php the_field('have_title'); ?>
          </h2>
          <ul class="have__list list-reset">
            <li class="have__item-1">
              <?php the_field('have_1'); ?>
            </li>

            <li class="have__item-2">
              <?php the_field('have_2'); ?>
            </li>

            <li class="have__item-3">
              <?php the_field('have_3'); ?>
            </li>

            <li class="have__item-4">
              <?php the_field('have_4'); ?>
            </li>

          </ul>
        </div>
      </div>
    </div>
  </section>

  <section class="idea">
    <div class="container">
      <div class="row">
        <div class="idea__content">
          <h2 class="idea__title">
            <?php the_field('idea_title'); ?>
          </h2>
          <p class="idea__descr">
            <?php the_field('idea_descr'); ?>
          </p>

          <div class="idea__contain">

            <?php
            // параметры по умолчанию
            $my_posts = get_posts(array(
              'numberposts' => -1,
              'category_name'    => 'idea"',
              'orderby'     => 'date',
              'order'       => 'ASC',
              'post_type'   => 'post',
              'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
            ));



            foreach ($my_posts as $post) {
              setup_postdata($post);
            ?>

              <div class="idea__card">
                <img class="idea_img" src="<?php the_field('idea_img'); ?>">
                <div class="idea__border"></div>
                <p class="idea__descr">
                  <?php the_field('idea_descr'); ?>
                </p>
              </div>

            <?php
            }

            wp_reset_postdata(); // сброс
            ?>

          </div>
        </div>
      </div>




    </div>
  </section>


</main>

<?php
get_footer();
?>