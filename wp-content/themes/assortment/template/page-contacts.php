<?php
/*
  Template Name: Контакты
  */
?>

<?php
get_header();
?>

<main class="page-main">

    <section class="contacts">
        <div class="container">
            <div class="row">
                <div class="contacts__content-top">
                    <h1 class="contacts__title">
                        <?php the_field('contacts_title'); ?>
                    </h1>
                </div>

                <div class="contacts__wrapp-grid">
                    <div class="contacts-card contacts-card--tel">
                        <div class="contacts-card__body">
                            <span class="contacts-card__title">Телефон</span>
                            <a class="contacts-card__contact" href="tel:+79128350288"><?php the_field('contacts_tel', 470); ?></a>
                        </div>
                    </div>
                    <div class="contacts-card contacts-card--mail">
                        <div class="contacts-card__body">
                            <span class="contacts-card__title">Почта</span>
                            <a class="contacts-card__contact"
                                href="mailto:assortiment45@mail.ru"><?php the_field('contacts_email_1', 470); ?></a>
                            <a class="contacts-card__contact"
                                href="mailto:mail@assortiment45.com"><?php the_field('contacts_email_2', 470); ?></a>
                        </div>
                    </div>
                    <div class="contacts-card contacts-card--geo">
                        <div class="contacts-card__body">
                            <span class="contacts-card__title">Адрес</span>
                            <address class="contacts-card__addres"><?php the_field('contacts_address', 470); ?></address>
                        </div>
                    </div>
                </div>


                <div class="contacts-maps__wrapp-grid">

                    <div class="contacts-maps__card">
                        <h2 class="contacts-maps__title"><?php the_field('contacts_title_left'); ?></h2>
                        <div class="contacts-map1">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/maps1.jpg" alt="">
                        </div>
                    </div>
                    <div class="contacts-maps__card">
                        <h2 class="contacts-maps__title"><?php the_field('contacts_title_right'); ?></h2>
                        <div class="contacts-map2">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/maps2.jpg" alt="">
                        </div>
                    </div>
                    <div class="contacts-maps__card">
                        <h2 class="contacts-maps__title"><?php the_field('contacts_title_form'); ?></h2>
                        <div class="contacts-map2">
                            <p class="contacts-maps__text"><?php the_field('contacts_descr_form'); ?></p>
                            <a class="contacts-maps__link" href="tel:+73522550288"><?php the_field('contacts_tel_form'); ?></a>
                            <a class="contacts-maps__link" href="https://wa.me/79128350288"><?php the_field('contacts_whatsapp_form'); ?>
                            </a>
                        </div>
                    </div>

                    <?php echo do_shortcode('[contact-form-7 id="656" title="Форма в контактах"]'); ?>

                </div>
            </div>
    </section>
</main>

<?php
get_footer();
?>