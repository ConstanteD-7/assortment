<?php
/*
  Template Name: Готовые лекала
  */
?>

<?php
get_header();
?>

<main class="page-main">

    <section class="patterns">
        <div class="container">
            <div class="row">
                <h1 class="patterns__title title">
                    <?php the_field('patterns_title'); ?>
                </h1>
                <p class="patterns__descr">
                    <?php the_field('patterns_descr'); ?>
                </p>
            </div>

            <div class="row">
                <div class="patterns__content">
                    <div class="patterns__content-left">
                        <img src="<?php the_field('patterns_img'); ?>">
                        <a class="patterns__btn" href="<?php the_field('patterns_file'); ?>" download>
                            <?php the_field('patterns_text_btn'); ?>
                        </a>
                    </div>
                    <p class="patterns__descr patterns__descr--mt col-lg-5"><?php the_field('patterns_descr-file'); ?>
                    </p>
                </div>
            </div>

        </div>
    </section>

    <!--     <section class="examples">
        <div class="container">
            <div class="row">
                <h2 class="examples__title title">
                    <?php the_field('examples_title'); ?>
                </h2>
            </div>

            <div class="row">
                <div class="examples__content">
                    <?php
                    // параметры по умолчанию
                    $my_posts = get_posts(array(
                        'numberposts' => -1,
                        'category_name'    => 'patterns',
                        'orderby'     => 'date',
                        'order'       => 'ASC',
                        'post_type'   => 'post',
                        'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                    ));

                    foreach ($my_posts as $post) {
                        setup_postdata($post);
                    ?>

                    <div class="examples__card">
                        <div class="examples__content-left">
                            <h3 class="examples__content-title">
                                <?php the_field('examples_card-title'); ?>
                            </h3>

                            <a class="examples__btn btn-link" href="#">
                                <?php the_field('examples_text-btn'); ?>
                            </a>
                        </div>

                        <div class="examples__content-right">
                            <img class="examples__img-1" src="<?php the_field('examples_img-1'); ?>">
                            <img class="examples__img-2" src="<?php the_field('examples_img-2'); ?>">
                        </div>
                    </div>

                    <?php
                    }

                    wp_reset_postdata(); // сброс
                    ?>
                </div>

            </div>
        </div>
    </section> -->


    <section class="examples">
        <div class="container examples__container">
            <div class="card-example">
                <sapn class="card-example__title">Платье «Мари»</sapn>
                <a href="#" class="card-example__link btn-link">Заказать</a>
                <!--                 <div class="card-example__img-wrapp"> -->
                <img class="examples__img-1"
                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Patterns/image28.jpg">
                <img class="examples__img-2"
                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Patterns/image27.jpg">
                <!-- </div> -->
            </div>
            <div class="card-example">
                <sapn class="card-example__title">Платье «Мари»</sapn>
                <a href="#" class="card-example__link btn-link">Заказать</a>
                <!--                 <div class="card-example__img-wrapp"> -->
                <img class="examples__img-1"
                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Patterns/image28.jpg">
                <img class="examples__img-2"
                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Patterns/image27.jpg">
                <!-- </div> -->
            </div>
            <div class="card-example">
                <sapn class="card-example__title">Платье «Мари»</sapn>
                <a href="#" class="card-example__link btn-link">Заказать</a>
                <!--                 <div class="card-example__img-wrapp"> -->
                <img class="examples__img-1"
                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Patterns/image28.jpg">
                <img class="examples__img-2"
                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Patterns/image27.jpg">
                <!-- </div> -->
            </div>

        </div>
    </section>

    <section class="advantages">
        <div class="container">
            <div class="row">
                <div class="advantages__content">
                    <h2 class="advantages__title col-lg-5 col-md-12">
                        <?php the_field('advantages_title'); ?>
                    </h2>
                    <ul class="advantages__list list-reset col-lg-7 col-md-12">
                        <li class="advantages__item-1">
                            <?php the_field('advantages_1'); ?>
                        </li>

                        <li class="advantages__item-2">
                            <?php the_field('advantages_2'); ?>
                        </li>

                        <li class="advantages__item-3">
                            <?php the_field('advantages_3'); ?>
                        </li>

                        <li class="advantages__item-4">
                            <?php the_field('advantages_4'); ?>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="form">
        <div class="container form__wrapp">
            <div class="row">
                <div class="form__container">
                    <div class="form__content">
                        <div class="form__content-left col-lg-6">
                            <h2 class="form__title">
                                <?php the_field('advantages_form_title'); ?>
                            </h2>
                            <p class="form__descr">
                                <?php the_field('advantages_form_text'); ?>
                            </p>

                            <a class="form__contact" href="tel:+73522550288"><?php the_field('advantages_form_tel'); ?></a>

                            <a class="form__contact" href="https://wa.me/79128350288"><?php the_field('advantages_form_whatsapp'); ?></a>
                        </div>
                        <div class="form__content-right col-lg-6">
                            <?php echo do_shortcode('[contact-form-7 id="184" title="Форма заявки"]'); ?>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </section>

</main>

<?php
get_footer();
?>
