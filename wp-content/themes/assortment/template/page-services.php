<?php
/*
  Template Name: Каталог услуг
  */
?>

<?php
get_header();
?>

<main class="page-main">

    <section class="catalog">
        <div class="container">
            <div class="row">
                <div class="catalog__content-top">
                    <h1 class="catalog__title">
                        <?php the_field('services_title'); ?>
                    </h1>
                    <p class="catalog__subtitle">
                        <?php the_field('services_subtitle'); ?>
                    </p>
                </div>

                <div class="catalog__content-bottom">
                    <?php
                    // параметры по умолчанию
                    $my_posts = get_posts(array(
                        'numberposts' => -1,
                        'category_name'    => 'services',
                        'orderby'     => 'date',
                        'order'       => 'ASC',
                        'post_type'   => 'post',
                        'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                    ));

                    foreach ($my_posts as $post) {
                        setup_postdata($post);
                    ?>
                        <a class="catalog__card-link" href="<?php echo get_permalink(); ?>">
                            <div class="catalog__card">

                                <img class="catalog_img" src="<?php the_field('services_img'); ?>">

                                <div class="catalog__content">
                                    <div class="catalog__border"></div>
                                    <p class="catalog__content-title">
                                        <?php the_field('services_card-title'); ?>
                                    </p>

                                    <a class="catalog__btn btn-link" href="<?php echo get_permalink(); ?>">
                                        <?php the_field('services_text-btn'); ?>
                                    </a>
                                </div>


                            </div>
                        </a>


                    <?php
                    }

                    wp_reset_postdata(); // сброс
                    ?>
                </div>
            </div>

        </div>
        </div>
    </section>

    <section class="form">
        <div class="container form__wrapp">
            <div class="row">
                <div class="form__container">
                    <div class="form__content">
                        <div class="form__content-left col-lg-6">
                            <h2 class="form__title">
                                <?php the_field('services_form_title'); ?>
                            </h2>
                            <p class="form__descr">
                                <?php the_field('services_form_text'); ?>
                            </p>

                            <a class="form__contact" href="tel:+73522550288"><?php the_field('services_form_tel'); ?></a>

                            <a class="form__contact" href="https://wa.me/79128350288"><?php the_field('services_form_whatsapp'); ?></a>
                        </div>
                        <div class="form__content-right col-lg-6">
                            <?php echo do_shortcode('[contact-form-7 id="184" title="Форма заявки"]'); ?>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </section>

    <section class="gallery">
        <div class="container">
            <div class="row">
                <h2 class="gallery__title">
                    <?php the_field('gallery_title'); ?>
                </h2>
            </div>

            <div class="row gallery__content">
                <div class="gallery__content-left">
                    <img class="gallery_img-1" src="<?php the_field('gallery_img-1'); ?>">
                    <img class="gallery_img-2" src="<?php the_field('gallery_img-2'); ?>">
                    <img class="gallery_img-3" src="<?php the_field('gallery_img-3'); ?>">
                </div>

                <div class="gallery__content-center">
                    <img class="gallery_img-4" src="<?php the_field('gallery_img-4'); ?>">
                    <img class="gallery_img-5" src="<?php the_field('gallery_img-5'); ?>">
                    <img class="gallery_img-6" src="<?php the_field('gallery_img-6'); ?>">
                </div>

                <div class="gallery__content-right">
                    <img class="gallery_img-7" src="<?php the_field('gallery_img-7'); ?>">
                    <img class="gallery_img-8" src="<?php the_field('gallery_img-8'); ?>">
                </div>
            </div>


        </div>

    </section>
</main>

<?php
get_footer();
?>