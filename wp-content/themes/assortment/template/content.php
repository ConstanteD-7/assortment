
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<?php

		wp_link_pages(
			array(
				'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'assortment' ) . '">',
				'after'    => '</nav>',
			)
		);


		?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
