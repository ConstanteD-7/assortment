<?php
/*
  Template Name: Отзывы
  */
?>

<?php
get_header();
?>

<main class="page-main">

  <section class="reviews">
    <div class="container">
      <div class="row load-more">
        <div class="reviews__content-top">
          <h1 class="reviews__title">
            <?php the_field('views_title'); ?>
          </h1>
          <p class="reviews__descr">
            <?php the_field('views_descr'); ?>
          </p>
        </div>

        <div class="reviews__wrapp-grid">

          <?php
          // параметры по умолчанию
          $my_posts = get_posts(array(
            'numberposts' => -1,
            'category_name'    => 'reviews',
            'orderby'     => 'date',
            'order'       => 'ASC',
            'post_type'   => 'post',
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
          ));

          foreach ($my_posts as $post) {
            setup_postdata($post);
          ?>

            <div class="reviews-card">
              <img src="<?php the_field('avatar'); ?>" class="reviews-card__img">
              <div class="reviews-card__content">
                <span class="reviews-card__title"><?php the_field('client_name'); ?></span>
                <span class="reviews-card__order"><?php the_field('client_order'); ?></span>
                <p class="reviews-card__text"><?php the_field('text_views'); ?></p>
              </div>
            </div>


          <?php
          }

          wp_reset_postdata(); // сброс
          ?>


        </div>

        <h2 class="reviews-video__title"><?php the_field('views_title_video'); ?></h2>
        <div class="reviews-video__wrapp-grid">

          <?php
          // параметры по умолчанию
          $my_posts = get_posts(array(
            'numberposts' => -1,
            'category_name'    => 'video_reviews',
            'orderby'     => 'date',
            'order'       => 'ASC',
            'post_type'   => 'post',
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
          ));

          foreach ($my_posts as $post) {
            setup_postdata($post);
          ?>

            <div class="reviews-video__card">
              <video class="reviews-video__card" width="585px" height="310px" controls loop preload="none" poster="<?php the_field('video_reviews_poster'); ?>">
                <source src="<?php the_field('video_reviews'); ?>" type="video/mp4">
              </video>
              <div class="reviews-video__content">
                <span class="reviews-video__sub-title"><?php the_field('video_reviews_name'); ?></span>
                <p class="reviews-video__desr"><?php the_field('video_reviews_order'); ?></p>
              </div>


            </div>


          <?php
          }

          wp_reset_postdata(); // сброс
          ?>

          <!-- не трогать <br>, опускает ниже кнопку плагина "показать еще" -->
          <br>

        </div>

      </div>
  </section>
</main>

<?php
get_footer();
?>
