<?php
/*
  Template Name: Сертификаты
  */
?>

<?php
get_header();
?>

<main class="page-main">

    <section class="certificate">
        <div class="container">
            <div class="row">
                <div class="contacts__content-top">
                    <h1 class="ordering__title">
                        <?php the_field('certificate_title'); ?>
                    </h1>
                    <p class="reviews__descr">
                        <?php the_field('certificate_descr'); ?>
                    </p>
                </div>

                <div class="certificate__container">


                    <?php
                    // параметры по умолчанию
                    $my_posts = get_posts(array(
                        'numberposts' => -1,
                        'category_name'    => 'certificate',
                        'orderby'     => 'date',
                        'order'       => 'ASC',
                        'post_type'   => 'post',
                        'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
                    ));

                    foreach ($my_posts as $post) {
                        setup_postdata($post);
                    ?>

                        <div class="certificate-card">
                            <img class="certificate-card__img" src="<?php the_field('certificate_img'); ?>">
                            <span class="certificate-card__title"><?php the_field('certificate_name'); ?></span>
                        </div>

                    <?php
                    }

                    wp_reset_postdata(); // сброс
                    ?>

                </div>
            </div>
    </section>
</main>

<?php
get_footer();
?>