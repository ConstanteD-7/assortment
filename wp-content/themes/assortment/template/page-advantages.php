<?php
/*
  Template Name: Наши преимущества
  */
?>

<?php
get_header();
?>

<main class="page-main">

    <section class="ordering">
        <div class="container">
            <div class="row">
                <div class="contacts__content-top">
                    <h1 class="ordering__title">
                        <?php the_field('advantages_title'); ?>
                    </h1>
                    <p class="reviews__descr">
                        <?php the_field('advantages_descr'); ?>
                    </p>
                </div>

                <div class="advantages__row row">
                    <div class="col-lg-6 col-md-6">
                        <div class="stage">
                            <div class="stage__header"><span>01</span>преимущество</div>
                            <p class="stage__text"><?php the_field('advantages_1'); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 advantages__img-wrapp row">
                        <div class="col-lg-8 col-md-8 advantages__img">
                            <img class="advantage__img"
                                src="<?php the_field('advantages_img_big_1'); ?>" alt="">
                        </div>
                        <div class="col-lg-4 col-md-4 advantages__img">
                            <img class="advantage__img--mini"
                                src="<?php the_field('advantages_img_small_1'); ?>">
                        </div>
                    </div>
                </div>
                <div class="advantages__row row">
                    <div class="col-lg-6 col-md-6">
                        <div class="stage">
                            <div class="stage__header"><span>02</span>преимущество</div>
                            <p class="stage__text"><?php the_field('advantages_2'); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 advantages__img-wrapp row">
                        <div class="col-lg-8 col-md-8 advantages__img">
                            <img class="advantage__img"
                                src="<?php the_field('advantages_img_big_2'); ?>">
                        </div>
                        <div class="col-lg-4 col-md-4 advantages__img">
                            <img class="advantage__img--mini"
                                src="<?php the_field('advantages_img_small_2'); ?>">
                        </div>
                    </div>
                </div>

                <div class="advantages__row row">
                    <div class="col-lg-6 col-md-6">
                        <div class="stage">
                            <div class="stage__header"><span>03</span>преимущество</div>
                            <p class="stage__text"><?php the_field('advantages_3'); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 advantages__img-wrapp row">
                        <div class="col-lg-8 col-md-8 advantages__img">
                            <img class="advantage__img"
                                src="<?php the_field('advantages_img_big_3'); ?>">
                        </div>
                        <div class="col-lg-4 col-md-4 advantages__img">
                            <img class="advantage__img--mini"
                                src="<?php the_field('advantages_img_small_3'); ?>">
                        </div>
                    </div>
                </div>

                <div class="advantages__row row">
                    <div class="col-lg-6 col-md-6">
                        <div class="stage">
                            <div class="stage__header"><span>04</span>преимущество</div>
                            <p class="stage__text"><?php the_field('advantages_4'); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 advantages__img-wrapp row">
                        <div class="col-lg-8 col-md-8 advantages__img">
                            <img class="advantage__img"
                                src="<?php the_field('advantages_img_big_4'); ?>">
                        </div>
                        <div class="col-lg-4 col-md-4 advantages__img">
                            <img class="advantage__img--mini"
                                src="<?php the_field('advantages_img_small_4'); ?>">
                        </div>
                    </div>
                </div>

                <div class="advantages__row row">
                    <div class="col-lg-6 col-md-6">
                        <div class="stage">
                            <div class="stage__header"><span>05</span>преимущество</div>
                            <p class="stage__text"><?php the_field('advantages_5'); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 advantages__img-wrapp row">
                        <div class="col-lg-8 col-md-8 advantages__img">
                            <img class="advantage__img"
                                src="<?php the_field('advantages_img_big_5'); ?>">
                        </div>
                        <div class="col-lg-4 col-md-4 advantages__img">
                            <img class="advantage__img--mini"
                                src="<?php the_field('advantages_img_small_5'); ?>">
                        </div>
                    </div>
                </div>

            </div>
    </section>
</main>

<?php
get_footer();
?>