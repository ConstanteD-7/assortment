<?php
/*
  Template Name: Шаблон карточки услуги
  Template Post Type: post
  */
?>


<?php
get_header();
?>

<?php
/* Start the Loop */
while (have_posts()) :
  the_post();

  get_template_part('template/content', get_post_type());


endwhile; // End of the loop.
?>

<body>
    <section class="card-services">
        <div class="container">
            <div class="row">
                <div class="col card-services__bread-crumbs">
                    <?php
          if (function_exists('yoast_breadcrumb')) {
            yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
          }
          ?>
                </div>

                <h1 class="card-services__title">
                    <?php the_field('card-services_title'); ?>
                </h1>

                <p class="card-services__descr">
                    <?php the_field('card-services_descr'); ?>
                </p>

                <img class="card-services__img" src="<?php the_field('card-services_img'); ?>">

                <p class="card-services__descr">
                    <?php the_field('card-services_descr-1'); ?>
                </p>

                <ul class="card-services__list list-reset">
                    <li class="card-services__item-1">
                        <?php the_field('card-services_time'); ?>
                    </li>

                    <li class="card-services__item-2">
                        <?php the_field('card-services_price'); ?>
                    </li>

                    <a class="card-services__btn btn-link" href="">
                        Заказать услугу
                    </a>

                </ul>

            </div>
        </div>
    </section>
</body>

<?php
get_footer();
?>
