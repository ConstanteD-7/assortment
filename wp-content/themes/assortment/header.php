<!doctype html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title><?php bloginfo('name');
            echo " | ";
            bloginfo('description'); ?></title>

    <?php
    wp_head();
    ?>
</head>

<body class="page">
    <header class="page-header">
        <div class="container">

            <div class="page-header__wrapper">
                <!-- Подключаем динамический логотип здесь и в футере -->
                <!-- ссылку замени на див -->
                <div class="page-header__logo">
                    <?php the_custom_logo(); ?>
                </div>

                <?php wp_nav_menu([
                    'container'       => 'nav',
                    'container_class' => 'page-header__nav nav',
                    'menu_class'      => 'nav__list list-reset',

                ]); ?>

                <div class="page-header__sub-burger-menu sub-burger-menu">
                    <div class="sub-burger-menu__bg"></div>
                    <button class="sub-burger-menu__button">
                        <span class="sub-burger-menu__line"></span>
                        <span class="sub-burger-menu__line"></span>
                        <span class="sub-burger-menu__line"></span>
                    </button>
                    <div class="sub-burger-menu__nav">
                        <ul class="sub-burger-menu__list list-reset">
                            <li class="sub-burger-menu__item">
                                <?php wp_nav_menu([
                                    'container'       => 'nav-mobile',
                                    'container_class' => 'sub-burger-menu__nav-mobile',
                                    'menu_class'      => 'nav__list list-reset',

                                ]); ?>
                            </li>
                            
                            <li class="sub-burger-menu__item"><a href="http://assortment/oformlenie-zakaza/" class="sub-burger-menu__link">Как оформить
                                заказ</a></li>
                            <li class="sub-burger-menu__item"><a href="http://assortment/nashi-preimushhestva/" class="sub-burger-menu__link">Преимущества</a>
                            </li>
                            <li class="sub-burger-menu__item"><a href="http://assortment/novosti/" class="sub-burger-menu__link">Новости</a>
                            </li>
                            <li class="sub-burger-menu__item"><a href="http://assortment/otzyvy/" class="sub-burger-menu__link">Отзывы</a>
                            </li>
                            <li class="sub-burger-menu__item"><a href="http://assortment/kontakty/" class="sub-burger-menu__link">Контакты</a>
                            </li>
                        </ul>
                        <div class="sub-burger-menu__wrapp">
                            <ul class="sub-burger-menu__list list-reset">
                                <li class="sub-burger-menu__item"><a href="tel:+78008008080" class="sub-burger-menu__link">8&nbsp;800&nbsp;800 80&nbsp;80</a></li>
                                <li class="sub-burger-menu__item"><a href="https://wa.me/78008008080" class="sub-burger-menu__link">WhatsApp</a></li>
                                <li class="sub-burger-menu__item"><a href="tg://msg?text=<?php echo urlencode('<TEXT>'); ?>&to=78008008080" class="sub-burger-menu__link">Telegram</a></li>
                                <li class="sub-burger-menu__item"><a href="#" class="sub-burger-menu__link">Email</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </header>
