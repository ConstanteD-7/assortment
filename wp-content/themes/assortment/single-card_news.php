<?php
/*
  Template Name: Шаблон карточки услуги
  Template Post Type: post
  */
?>


<?php
get_header();
?>

<?php
/* Start the Loop */
while (have_posts()) :
  the_post();

  get_template_part('template/content', get_post_type());


endwhile; // End of the loop.
?>

<body>
    <section class="card-news">
        <div class="container">
            <div class="row">
                <div class="col card-news__bread-crumbs">
                    <?php
          if (function_exists('yoast_breadcrumb')) {
            yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
          }
          ?>
                </div>

                <h1 class="card-news__title">
                    <?php the_field('card-new_title'); ?>
                </h1>

                <p class="card-news__descr">
                    <?php the_field('card-new_descr'); ?>
                </p>

                <img class="card-news__img" src="<?php the_field('card-new_img'); ?>">

                <p class="card-news__descr">
                    <?php the_field('card-new_descr-1'); ?>
                </p>

            </div>
        </div>
    </section>
</body>

<?php
get_footer();
?>