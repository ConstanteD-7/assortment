<?php
  get_header();
?>

<main class="page-main">
    <section class="page-404">
        <div class="container">
            <h1 class="page-404__title">Ошибка 404<?php the_field('page-404_title'); ?></h1>
            <p class="page-404__descr">Что-то пошло не так! Пожалуйста, обновите страницу или проверьте доступ к
                Интернету</p>
            <div class="page-404__wrapp">

                <a href="#" class="page-404__btn btn-link">Вернуться на главную
                    страницу<?php the_field('page-404_btn'); ?></a>
            </div>
        </div>
    </section>

</main>

<?php
  get_footer();
?>
