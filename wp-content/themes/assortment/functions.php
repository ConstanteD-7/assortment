<?php

  // Добавляем действия
  add_action('wp_enqueue_scripts', 'assortment_scripts');

  // Подключаем файл со стилями
  function assortment_scripts() {
    
    wp_enqueue_style( 'assortment-normalize', get_stylesheet_directory_uri() . '/assets/css/normalize.css');
/*     wp_enqueue_style( 'assortment-bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap-grid.min.css'); */
    wp_enqueue_style( 'assortment-bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_enqueue_style( 'assortment-styles', get_stylesheet_uri() );
    wp_enqueue_script( 'assortment-scripts', get_template_directory_uri() . '/assets/js/index.js', array(), NULL, true);
    wp_enqueue_script( 'assortment-scripts', get_template_directory_uri() . '/assets/js/play-btn.js', array(), NULL, true);
    wp_enqueue_style('fonts');
  };


  // Добавляет в админке функцию выбрать логотип. Не забудь в хедере прописать подключение этого лого.
  add_theme_support( 'custom-logo');
  add_theme_support('widgets');
  add_theme_support( 'wp-block-styles');
  add_theme_support( 'core-block-patterns');

  //подключаем меню навигации

  add_action( 'after_setup_theme', 'theme_register_nav_menu' );

  function theme_register_nav_menu() {
    register_nav_menu( 'primary', 'Primary Menu' );
  }

  //classes all menu

  add_filter( 'nav_menu_css_class', 'add_my_class_to_nav_menu', 10, 2 );
  function add_my_class_to_nav_menu( $classes, $item ){


    $classes[] = 'my__class';

    return $classes;
  }

?>