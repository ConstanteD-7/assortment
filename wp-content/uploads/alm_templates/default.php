<div class="reviews-card">
	<img src="<?php the_field('avatar'); ?>"
     class="reviews-card__img">
	<div class="reviews-card__content">
		<span class="reviews-card__title"><?php the_field('client_name'); ?></span>
		<span class="reviews-card__order"><?php the_field('client_order'); ?></span>
		<p class="reviews-card__text"><?php the_field('text_views'); ?></p>
	</div>
</div>
<br>