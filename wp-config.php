<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать файл в "wp-config.php"
 * и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки базы данных
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры базы данных: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'assortment' );

/** Имя пользователя базы данных */
define( 'DB_USER', 'Constanta' );

/** Пароль к базе данных */
define( 'DB_PASSWORD', 'Constanta' );

/** Имя сервера базы данных */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу. Можно сгенерировать их с помощью
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}.
 *
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными.
 * Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'C2#Miw:lCI(]?M@IPN*![ZOL/-vI=HB5DFGRh/cFw5,@gYi}KeS:l=;AuSzIRR$}' );
define( 'SECURE_AUTH_KEY',  '6|j)<QziPgiOOx/TGYD=xM#(!LRn3dliR:;1b<j+fOt7x`R`xBPD[/La![I0|OaY' );
define( 'LOGGED_IN_KEY',    'Xk7x-q](=+q!TT7uj`M^6a*21B#}5NLwhf8`riyOnj9Ep4ilGf9]ks?QG*j6KP;|' );
define( 'NONCE_KEY',        'HAf=/v?]!inr8M,pCC!c+XZC9zPaol7A[$Nn?AvK(H2!<w&xBPqvH6W<Mw@;#aWE' );
define( 'AUTH_SALT',        'ZVmsa]7$*+%vZd|m.e=.XvpadSHM_Xwc?:BrzR=L2{vD.YT+.-s;*+|9!w7]<FE)' );
define( 'SECURE_AUTH_SALT', '))C`c%:nsfF8L;^prh@vPy0[v.c+9WGBz/%$FL?#ke`7xzq~)VJS*j+:u)(_%esg' );
define( 'LOGGED_IN_SALT',   '~G#g+0FZM+!i{V^x>8f;f-.d]&?*(5=$W5!^jpP4X~yM,;-K7Z^CvV]}jjJtc>l{' );
define( 'NONCE_SALT',       '>I}1QvIWK1(e@PT|48.c=bQoNzhWJ1Pp/eRHG9% L35kk=rIl^cG:gs?fW?1TXFW' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Произвольные значения добавляйте между этой строкой и надписью "дальше не редактируем". */



/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
